// number 2
db.fruits.aggregate( [
	{$match :{ "onSale": true }},
	{ $count: "fruitsOnSale" }
] );

// number 3
db.fruits.aggregate([
	{ $match: { stock: {$gt: 20} } },
	{ $count: "moreStock" }
]);

// number 4
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "avgprice": { $avg: "$price" } } }
]);

// number 4
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "maxprice": { $max: "$price" } } }
]);
// number 5
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "minprice": { $min: "$price" } } }
]);